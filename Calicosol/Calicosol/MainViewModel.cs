﻿using System.Windows.Input;
using Xamarin.Forms;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Calicosol
{
    public class Person
    {
        public string Name { get; set; }
    }
    public class MainViewModel : ViewModelBase
    {
        private string _search;
        private List<Person> _people;

        public MainViewModel(DemoApp app) : base(app)
        {
           _people = new List<Person>();
        }
        
        public string Search
        {
            get { return _search; }
            set
            {
                if (_search != value)
                {
                    _search = value;
                    OnPropertyChanged();
                    UpdateSearchResults();
                }
            }
        }

        private void UpdateSearchResults()
        {
            if (this.Search.Length < 3)
            {
                this.People = new List<Person>();
                return;
            }
            var length = 200 - (this.Search.Length * 10);
            var list = new List<Person>();
            for (int i = 0; i < length; i++)
            {
                list.Add(new Person() { Name = $"Person {i}" });
            }
            this.People = list;
        }

        public List<Person> People
        {
            get
            {
                return _people;
            }
            set
            {
                _people = value;
                OnPropertyChanged();
            }
        }
    }
}
