﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Calicosol
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public ViewModelBase(DemoApp app)
        {
            this.Application = app;
        }

        public DemoApp Application { get; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}